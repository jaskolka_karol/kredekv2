import numpy as np
import conversion as conv
from PIL import Image 

def noiseCreator(array):

    table = conv.decToBin(array)

    for i in range(8):
        # sygnal jest zaklocony wiec bity sa losowe
        table[i] = np.random.randint(0,2)

    return conv.binToDec(table)

def simulation(image_name, changeToError, changeToRight):

    img = Image.open(image_name) # zaimportowanie obrazka

    img_array = np.array(img) # obraz jako tablica numPy

    print("Przesylanie obrazka przez model Gilberta ...")

    # przesylany obraz ma wymiary Fruit.bmp 256x256

    noise = False

    for i in range(256):
        for j in range(256):

            # jesli sygnal jest w stanie zaklocenia
            if(noise == True):
                img_array[i][j] = noiseCreator(img_array[i][j])
                rand = np.random.randint(changeToRight) 
                if(rand == 0):
                    noise = not noise
            else:
            # jesli sygnal jest w stanie poprawnym
                rand = np.random.randint(changeToError) 
                if(rand == 0):
                    noise = not noise
                    
    img = Image.fromarray(img_array) # zamiana tablicy numPy na obraz

    print("Gotowe")

    img.save('gilbert_out.png', 'PNG') # zapis obrazu