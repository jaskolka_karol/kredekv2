import math
import numpy as np
import conversion as conv
from PIL import Image 

# dodanie bitu parzystosci
# zwraca tablice 9 bitowa po dodaniu bitu do tablicy 8 bitowej
def addBit(number):
    
    # konwersja liczby na system binarny
    array = conv.decToBin(number)
    # nowa tablica
    newArray = [0,0,0,0,0,0,0,0,0]
    # licznik jedynek
    oneCounter = 0
    # przekopiowanie 8 bitow
    for i in range(8):
        newArray[i] = array[i]
        if(newArray[i] > 0):
            # zliczenie jedynek
            oneCounter = oneCounter + 1
    # dodanie bitu parzystosci na ostatnim miejscu [8]
    if oneCounter%2 == 0:
        newArray[8] = 0
    else:
        newArray[8] = 1
    # zwrocenie tablicy 9 bitowej (8 bitow informacji + bit parzystosci)
    return newArray

# symulacja przesylania tablicy dopoki bit parzystosci nie bedzie sie zgadzal
def sendArray(array,error):
    copyArray = array
    oneCounter = 0
    # symulacja zaklocenia
    for i in range(9):
        # Binary Symetric Channel
        # Prawdopodobienstwo bledu = 1/error
        rand = np.random.randint(error) 
        if(rand == 0): 
            if(array[i] == 0):
                array[i] = 1
            else:
                array[i] = 0
        if array[i] > 0:  
            if i < 8: # bit parzystości nie jest dodawany do licznika
                oneCounter = oneCounter + 1 
    if oneCounter%2 == 0:   
        bit = 0 
    else:
        bit = 1  
    # sprawdzenie czy bit parzystości jest poprawny
    if(bit != array[8]):
        # retransmisja
        return sendArray(copyArray,error)
    else:
        # koniec zwrocenie tablicy i konwersja na system dziesietny
        return conv.binToDec(array)

def simulation(image_name,error):

    img = Image.open(image_name) # zaimportowanie obrazka

    img_array = np.array(img) # obraz jako tablica numPy

    print("Przesylanie obrazka przez BSC ...")

    # przesylany obraz ma wymiary Fruit.bmp 256x256

    for i in range(256):
        for j in range(256):
            
            # dodanie bitu parzystosci
            # symulacja przeslania

            img_array[i][j] = sendArray(addBit(img_array[i][j]), error)

    img = Image.fromarray(img_array) # zamiana tablicy numPy na obraz

    print("Gotowe")

    img.save('bsc_out.png', 'PNG') # zapis obrazu