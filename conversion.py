import math

# zamiana liczby dziesietnej 0-255 na binarna (8 bitowa w postaci tablicy)

def decToBin(number):
    
    # tablica 8 zer
    array = [0,0,0,0,0,0,0,0]
    # konwersja
    for counter in range(8):
        # sprawdzenie czy liczba jest podzielna przez 2
        if(number%2 != 0):
            # uzupelnianie tablicy od konca
            array[7-counter] = 1
        # czesc calkowita z podzielenia liczby przez 2
        number = math.floor(number/2)
    # zwrocenie liczby binarnej jako tablicy 8 bitow
    return array


# zamiana liczbe binarnej (pierwsze 8 bitow podane jako tablica) na liczbe dziesietna 0-255

def binToDec(array):
    # wynik
    result = 0
    # wartosc bedzie odczytywana od poczatku tablicy 
    # wartosc najstarszego bitu 2^7 = 128
    value = 128
    for i in range(8):
        # jesli 1
        if(array[i] == 1):
            # dodanie do wyniku odpowiedniej wartosci
            result = result + value
        # mlodszy bit = wartosc podzielona przez 2
        value = value/2
    # zwrocenie wyniku jako liczby calkowitej
    return math.floor(result)